const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const EventSchema = new Schema({
  date: {
    type: Date,
    validate: {
      validator: value => {
        return (
          value && value.getTime() >= Date.now() - 24 * 60 * 60 * 1000
        )
      },
      message: 'Нельзя выбрать дату позже сегодняшней'
    },
    required: true
  },
  title: {
    type: String,
    required: true
  },
  duration: {
    type: Number,
    min: [0.1],
    required: true
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  }
});

const Event = mongoose.model('Event', EventSchema);

module.exports = Event;
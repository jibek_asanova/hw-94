const express = require('express');
const Event = require('../models/Event');
const auth = require("../middleware/auth");
const router = express.Router();

router.get('/', auth, async (req, res) => {

  try {
    const userId = {};

    if (req.query.userId) {
      userId.userId = req.query.userId;

    }
    const events = await Event.find(userId).sort({date: 1});
    res.send(events);
  } catch (e) {
    res.status(500).send({error: 'Internal Server Error'});
  }
});

router.post('/', async (req, res) => {
  try {
    const eventData = {
      date: req.body.date,
      title: req.body.title,
      duration: req.body.duration || null,
      userId: req.body.userId
    };


    const event = new Event(eventData);
    await event.save();
    res.send(event);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const event = await Event.findByIdAndDelete(req.params.id);

    if (event) {
      return res.send(`event ${event.title} removed`);
    } else {
      return res.status(404).send({error: 'Event not found'})
    }
  } catch (e) {
    res.status(500).send('Invalid error');
  }
});


module.exports = router;

import React, {useState} from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {createEventRequest} from "../../store/actions/eventsActions";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
    alert: {
        marginTop: theme.spacing(3),
        width: "100%"
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const AddEvent = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.events.createEventError);
    const loading = useSelector(state => state.events.createEventLoading);
    const user = useSelector(state => state.users.user)

    const [state, setState] = useState({
        date: "",
        title: "",
        duration: "",
    });

    const submitFormHandler = async e => {
        e.preventDefault();
        await dispatch(createEventRequest({...state, userId: user._id}))
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    }

    return (
        <>
            <Typography variant="h4">Add Event</Typography>
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                className={classes.root}
                autoComplete="off"
                onSubmit={submitFormHandler}
                noValidate
            >
                <FormElement
                    label="Date"
                    name="date"
                    type="date"
                    value={state.date}
                    onChange={inputChangeHandler}
                    required
                    error={getFieldError('date')}

                />
                <FormElement
                    label="Title"
                    name="title"
                    value={state.title}
                    onChange={inputChangeHandler}
                    required
                    error={getFieldError('title')}
                />

                <FormElement
                    label="Duration"
                    type="number"
                    name="duration"
                    value={state.duration}
                    onChange={inputChangeHandler}
                    error={getFieldError('duration')}
                />
                <Grid item xs={12}>
                    <ButtonWithProgress
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        loading={loading}
                        disabled={loading}
                    >
                        Create
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </>
    );
};

export default AddEvent;
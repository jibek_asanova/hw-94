import React, {useEffect} from 'react';
import {Button, CircularProgress, Grid, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchEventRequest} from "../../store/actions/eventsActions";
import PostItem from "../../components/PostItem/PostItem";

const Events = () => {
    const dispatch = useDispatch();
    const events = useSelector(state => state.events.events);
    const fetchLoading = useSelector(state => state.events.fetchLoading);
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        dispatch(fetchEventRequest(user._id));
    }, [dispatch, user._id]);


    return (
        <div>
            <Grid container direction="column" spacing={2}>
                <Grid item container justifyContent="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="h4">Events</Typography>
                    </Grid>
                    <Grid item>
                        <Button variant="contained" component={Link} to="/events/new">Add event</Button>
                    </Grid>
                </Grid>
                <Grid item container direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) :events.map(event => (
                        <PostItem
                            key={event._id}
                            id={event._id}
                            title={event.title}
                            dateTime={event.date}
                            duration={event.duration}
                        />
                    ))}
                </Grid>
            </Grid>
        </div>
    );
};

export default Events;
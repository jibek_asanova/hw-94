import {Button, Card, CardActions, CardContent, CardHeader, Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {deleteEventRequest} from "../../store/actions/eventsActions";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%' // 16:9
    }
});

const PostItem = ({title, dateTime,duration, id}) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const date = new Date(dateTime);

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardContent>
                    <Typography variant="subtitle1">
                        Date: {date.toLocaleString()}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Typography variant="subtitle1">
                        Duration: {duration}
                    </Typography>
                </CardActions>
                <Button variant="contained" onClick={() => dispatch(deleteEventRequest(id))}>Delete</Button>
            </Card>
        </Grid>
    );
};

export default PostItem;
import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";
import {
    createEventFailure,
    createEventRequest,
    createEventSuccess, deleteEventFailure, deleteEventRequest, deleteEventSuccess, fetchEventFailure,
    fetchEventRequest,
    fetchEventSuccess
} from "../actions/eventsActions";

export function* createEventSagas({payload: eventData}) {
    try {
        yield axiosApi.post('/events', eventData);
        yield put(createEventSuccess());
        historyPush('/');
        toast.success('Event created');
    } catch (e) {
        yield put(createEventFailure(e.response.data));
        toast.error('Could not create event');
    }
}

export function* fetchEventsSagas({payload}) {
    try {
        const response = yield axiosApi.get(`/events?userId=${payload}` );
        yield put(fetchEventSuccess(response.data));
    } catch (error) {
        yield put(fetchEventFailure(error.response.data));
        toast.error('Could not fetch events');
    }
}

export function* deleteEvent(id) {
    try {
        yield axiosApi.delete('events/' + id.payload);
        yield put(deleteEventSuccess(id.payload));
        toast.success('Event deleted');
    } catch (error) {
        yield put(deleteEventFailure(error.response.data));
        toast.error('Could not delete event');
    }
}

const eventsSaga = [
    takeEvery(createEventRequest, createEventSagas),
    takeEvery(fetchEventRequest, fetchEventsSagas),
    takeEvery(deleteEventRequest, deleteEvent),
];

export default eventsSaga;
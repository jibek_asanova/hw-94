import {put, takeEvery} from "redux-saga/effects";
import {
    googleLoginRequest,
    loginUserFailure,
    loginUserRequest,
    loginUserSuccess,
    logoutUser,
    registerUser,
    registerUserFailure,
    registerUserSuccess
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";


export function* registerUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users', userData);
        yield put(registerUserSuccess(response.data));
        historyPush('/');
        toast.success('Registered successful!');
    } catch (error) {
        toast.error(error.response.data.global);
        yield put(registerUserFailure(error.response.data));
    }
}

export function* loginUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users/sessions', userData);
        yield put(loginUserSuccess(response.data.user));
        historyPush('/');
    } catch (error) {
        toast.error(error.response.data.global);
        yield put(loginUserFailure(error.response.data));
    }
}

export function* logoutUserSaga() {
    try {
        yield axiosApi.delete('/users/sessions');
        historyPush('/');
    } catch (error) {
        toast.error(error.response.data.global);
    }
}

export function* googleLoginSaga({payload: googleData}) {
    try {
        const response = yield axiosApi.post('/users/googleLogin', {
            tokenId: googleData.tokenId,
            googleId: googleData.googleId,
        });
        yield put(loginUserSuccess(response.data.user));
        historyPush('/');
    } catch (error) {
        toast.error(error.response.data.global);
        yield put(loginUserFailure(error.response.data));
    }
}

const usersSaga = [
    takeEvery(registerUser, registerUserSaga),
    takeEvery(loginUserRequest, loginUserSaga),
    takeEvery(logoutUser, logoutUserSaga),
    takeEvery(googleLoginRequest, googleLoginSaga),
];

export default usersSaga;
import usersSlice from "../slices/usersSlice";

export const {
    registerUser,
    registerUserSuccess,
    registerUserFailure,
    loginUserRequest,
    loginUserSuccess,
    loginUserFailure,
    clearErrorUser,
    logoutUser,
    googleLoginRequest,
} = usersSlice.actions;


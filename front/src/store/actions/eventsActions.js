import eventsSlice from "../slices/eventsSlice";

export const {
    createEventRequest,
    createEventSuccess,
    createEventFailure,
    fetchEventRequest,
    fetchEventSuccess,
    fetchEventFailure,
    deleteEventSuccess,
    deleteEventRequest,
    deleteEventFailure
} = eventsSlice.actions;
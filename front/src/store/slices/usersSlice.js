import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  user: null,
  registerLoading: false,
  registerError: null,
  loginError: null,
  loginLoading: false,
};

const name = 'users';

const usersSlice = createSlice({
  name,
  initialState,
  reducers: {
    registerUser(state) {
      state.registerLoading = true;
    },
    registerUserSuccess(state, {payload: userData}) {
      state.user = userData;
      state.registerLoading = false;
      state.registerError = null;
    },
    registerUserFailure(state, action) {
      state.registerLoading = false;
      state.registerError = action.payload;
    },
    loginUserRequest(state) {
      state.loginLoading = true;
    },
    loginUserSuccess(state, {payload: user}) {
      state.loginError = null;
      state.loginLoading = false;
      state.user = user;
    },
    loginUserFailure(state, {payload: loginError}) {
      state.loginError = loginError;
      state.loginLoading = false;
    },
    clearErrorUser(state) {
      state.registerError = null;
      state.loginError = null;
    },
    logoutUser(state){
      state.user = null;
    },
    googleLoginRequest(state, {payload: googleData}) {
      state.user = googleData;
    }
  }
});

export default usersSlice;
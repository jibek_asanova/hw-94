import {createSlice} from "@reduxjs/toolkit";

const name = 'events';
export const initialState = {
    fetchLoading: false,
    singleLoading: false,
    events: [],
    createEventLoading: false,
    createEventError: null
};

const eventsSlice = createSlice({
    name,
    initialState,
    reducers: {
        createEventRequest(state) {
            state.createEventLoading = true;
        },
        createEventSuccess(state) {
            state.createEventLoading = false;
            state.createEventError = null;
        },
        createEventFailure(state, {payload: error}) {
            state.createEventLoading = false;
            state.createEventError = error;
        },
        fetchEventRequest(state) {
            state.fetchLoading = true;
        },
        fetchEventSuccess(state, {payload: events}) {
            state.fetchLoading = false;
            state.events = events;
        },
        fetchEventFailure(state) {
            state.fetchLoading = false;
        },
        deleteEventSuccess(state, {payload: eventId}) {
            state.events = state.events.filter(event => event._id !== eventId);
        },
        deleteEventRequest(state) {
            state.fetchLoading = false;
        },
        deleteEventFailure(state) {
            state.fetchLoading = false;
        }
    }
})

export default eventsSlice;
import {all} from 'redux-saga/effects';
import eventsSaga from "./sagas/eventsSagas";
import usersSaga from "./sagas/usersSagas";

export function* rootSagas() {
  yield all([
      ...usersSaga,
      ...eventsSaga,
  ])
}